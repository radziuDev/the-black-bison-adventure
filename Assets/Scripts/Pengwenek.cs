using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pengwenek : MonoBehaviour
{
    Rigidbody rb;
    float moveTime;
    public float speed = 3f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + (transform.forward * speed * Time.deltaTime));
        moveTime += Time.deltaTime;
        if (moveTime > 5f)
        {
            transform.forward = transform.forward * -1;
            moveTime = 0;
        }
    }
}