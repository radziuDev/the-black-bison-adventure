using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class TextAnimation : MonoBehaviour
{
    public bool disabledModule = false;
    private string currentText;
    public float delay = 0.05f;
    public Image winScreen;

    public TextMeshProUGUI output;
    public RawImage box;

    public RawImage damian;
    public RawImage claudia;
    public RawImage krzychu;

    private int currentDialog = 0; 
    private string[][][] alldialogs = new string[][][]{
        new string[][]{
            new string[]{"O kurde, znowu się najebałem. Gdzie ja jestem?", "damian"},
            new string[]{"Cześć Damian, znowu zachlałeś i wywiało cię na koniec wioski.", "claudia"},
            new string[]{"Ooo to wszystko tłumaczy. A czekaj, kim ty wogóle jesteś?", "damian"},
            new string[]{"Jestem Claudia, zainstalowałeś mnie wczoraj jako swój motyw w visual studio.", "claudia"},
            new string[]{"Kurde, chyba czas przestać pić w pracy. Wiesz gdzie podział się Krzychu?", "damian"},
            new string[]{"Tak, po drugiej stronie wioski. Aby tam dotrzeć pójdź w prawo. Możesz użyć do tego klawiszy \'WSAD\'." , "claudia"},
            new string[]{"Ok." , "damian"},
        },
        new string[][]{
            new string[]{"Aby porozmawiać z krzychem podejdź do niego i użyj klawisza \"F\"" , "claudia"},
        },
         new string[][]{
            new string[]{"*Wychodzi z siana* KURŁA!!", "krzychu" },
            new string[]{"Widzę że też piłeś, nadal to rozpamiętujesz?", "damian" },
            new string[]{"Tak, wydawało mi się, że się uda a zobacz co się stało. Nasza gra jest w rankingu 10 gier ale nie najlepszych tylko crapów u błazna NRGeeka.", "krzychu"  },
            new string[]{"Teraz zobacz, ja śpię w sianie, a ty? A ty jesteś sobą i oboje musimy pracować w CDV i użerać się z tymi debilami którzy tworzą tą grę. ", "krzychu"  },
            new string[]{"Jedny pozytyw który wyszedł z tego że zostałem na lodzie to to, że zostałem największym eksporterem pingwinów na całym świecie, a nawet lepiej, w naszej wiosce.", "krzychu"  },
            new string[]{"Krzychu, co ty mówisz jakie pingwiny ?!?", "damian"  },
            new string[]{"O KURŁA!! Moje pingwiny. ZNIKNEŁY WSZYSTKIE! CAŁĘ 10! MUSISZ MI POMÓC JE ZNALEŹĆ.", "krzychu"  },
            new string[]{"Nie mogłeś kupić sobie kota jak normalny człowiek?", "damian"  },
            new string[]{"Damian, nie czas na dyrdymały. Ja sprawdzę miasto a ty przeszukaj las. Powinnem jednak był kupić ten elektryczny pastuch." , "krzychu" },
        },
         new string[][]{
            new string[]{"Hej uważaj! Wchodzisz do lasu, bywa tu niebezpiecznie." , "claudia"},
            new string[]{"Hmm, ok. Mam jakieś możliwości zapewnienia sobie bezpieczeństwa." , "damian"},
            new string[]{"Pewnie jak to każdy alkoho... pracownik sektoru Game Developmentu posiadasz wrodzony talent do walki.", "claudia" },
            new string[]{"Gdy użyjesz LPM zajebiesz oponetowi z twojej mechanicznej klawiatury.", "claudia" },
            new string[]{"Zato PPM podpali odory z w twojej mord...buzi i stworzy fireball'a." , "claudia"},
            new string[]{"Z fireball'ami może być ciężko jeszcze nie ma 12:00." , "damian"},
            new string[]{"Nic się nie bój. Wyposażyłam cię w zapas magicznych butelek, które pomogą ci 'odświerzyć' oddech." , "claudia"},
            new string[]{"Aby zaczerpnąć eliksiru kliknij 'E', a poziom toksyn zwiększy się i da możliwość stania się magiem ognia." , "claudia"},
            new string[]{"Pusta butelka to niezłe ammo, kwiatek to przesada ale jak wywalisz komuś z butli w łeb to też go złożysz." , "claudia"},
            new string[]{"Ok, ruszam." , "damian"},
        },
        new string[][]{
            new string[]{"Patrz pingwiny. Podbiegnij do nich i użyj 'F', aby schować je w kieszeń." , "claudia"},
            new string[]{"Jak w kieszń? Halo instrukcje są niejasne!" , "damian"},
        },
        new string[][]{
            new string[]{"KURŁA, Damian!! Sprowadziłeś wszystkich Oskarków. Ostarek 1, Oskarek 2, Oskarek 3, Oskarek 4, Oskarek 5..... Moje małe dzieci!" , "krzychu"},
            new string[]{"A po czym ty ich poznajesz?" , "damian"},
            new string[]{"Jak to po czym? Głupie pytanie, po numerze.", "krzychu" },
            new string[]{"Teraz gdy już mamy wszystkich w zagrodzie możemy w końcu się napić.", "krzychu" },
            new string[]{"Oj tak, 'the black bison' był by miłym podsumowaniem dnia.", "damian" },
            new string[]{"Ta, po tym fiasku z aztekami niestać mnie nawet na darmowe assety w unity.", "krzychu" },
            new string[]{"Dobra, chodź na rynek coś się wymyśli." , "damian"},
        },
    };
    public bool isDialog = false;
    private string[][] dialog = new string[][] {};
    int dialogRow = 0;

    private Coroutine coroutine;
    
    void Start()
    {
        StartDialog(0);
    }

    public void StartDialog(int number)
    {
        dialogRow = 0;
        currentDialog = number;
        dialog = alldialogs[currentDialog];
        coroutine = StartCoroutine(ShowText(dialog[dialogRow]));
    }

    private void Update()
    {
        if (disabledModule) return;
        if(Input.GetKeyDown(KeyCode.Space) && isDialog)
        {
            krzychu.gameObject.SetActive(false);
            claudia.gameObject.SetActive(false);
            damian.gameObject.SetActive(false);
            if (dialogRow >= dialog.Length)
            {
                StopCoroutine(coroutine);
                currentText = "";
                dialogRow = 0;
                isDialog = false;
                box.gameObject.SetActive(false);

                if (currentDialog == 5)
                {
                    disabledModule = true;
                    winScreen.gameObject.SetActive(true);
                    isDialog = true;
                    StartCoroutine(ExecuteAfterDelay(5f));
                }
            } 
            else 
            {
                StopCoroutine(coroutine);
                coroutine =  StartCoroutine(ShowText(dialog[dialogRow]));
            }
        }
    }

    IEnumerator ExecuteAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("MainMenu");        
    }


    private IEnumerator ShowText(string[] test)
    {
        string fullText = test[0];

        if (test[1] == "damian") {
            damian.gameObject.SetActive(true);
        }

        if (test[1] == "claudia")
        {
            claudia.gameObject.SetActive(true);
        }

        if (test[1] == "krzychu")
        {
            krzychu.gameObject.SetActive(true);
        }

        isDialog = true;
        dialogRow++;
        currentText = "";
        int count = 0;
        output.text = currentText;
        box.gameObject.SetActive(true);

        while (count < fullText.Length)
        {   
            currentText += fullText[count];
            output.text = currentText;
            count++;
            yield return new WaitForSeconds(delay);
        }
    }
}
