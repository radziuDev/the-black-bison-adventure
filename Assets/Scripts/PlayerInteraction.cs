using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField] private LayerMask mask;
    public int pengewenkiCount;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) {
            Interaction();
        }
    }

    void Interaction()
    {
        bool didHit;
        RaycastHit hit;
        didHit = Physics.Raycast(transform.position, transform.forward, out hit, 3f, mask);
        if(didHit && hit.collider.tag == "Pengwenek")
        {
            Destroy(hit.transform.gameObject);
            pengewenkiCount++;
        }
    }
}
