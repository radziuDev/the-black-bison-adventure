using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtackSystem : MonoBehaviour
{
    private Statistics statistics;
    private Animator animator;
    public float cooldownTime = 2f;
    private float cooldownTimer = 0f;
    private bool animationReady = true;

    public GameObject bootlePrefab;
    public float throwBottleForce = 10f;
    public float destroyBottleTime = 20f;

    public GameObject flamePrefab;
    public float throwFlameForce = 30f;
    public float destroyFlameTime = 0.2f;
    public TextAnimation textManager;

    void Start()
    {
        statistics = GetComponent<Statistics>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (textManager.isDialog) return;
        if (!animationReady)
        {
            cooldownTimer -= Time.deltaTime;
            if (cooldownTimer <= 0)
            {
                animationReady = true;
            }
        }

        if (!animationReady) return;

        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            Mele();
        }
    
        if (Input.GetKeyDown(KeyCode.Mouse1)) {
            Spell();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            Drink();
        }

         if (Input.GetKeyDown(KeyCode.Q)) {
            Throw();
        }
    }

    public void Mele() {
        if (statistics.CanIUseStamina(10)) {
            statistics.ChangeStamina(-10);
            animator.SetTrigger("Mele");
        }
    }

    public void Spell() {
        if (statistics.CanIUseToxicity(30)) {
            statistics.ChangeToxicity(-30);

            // animator.SetTrigger("Spell");
            Vector3 spawnPosition = transform.position + transform.up + (transform.forward * 1f);
            GameObject newObj = Instantiate(flamePrefab, spawnPosition, transform.rotation);
            Rigidbody rb = newObj.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = transform.up * 1f;
                rb.AddForce(transform.forward * throwFlameForce, ForceMode.Impulse);
            }

            Destroy(newObj, destroyFlameTime);
        }
    }

    public void Drink() {
        if (statistics.CanIDrink(1)) {
            statistics.Drink(1);
            statistics.ChangeToxicity(30);
            // animator.SetTrigger("Drink");
        }
    }

    public void Throw() {
        if (statistics.CanIThrow(1)) {
            statistics.Throw(1);
            // animator.SetTrigger("Drink");

            Vector3 spawnPosition = transform.position + transform.up + (transform.forward * 2f);
            GameObject newObj = Instantiate(bootlePrefab, spawnPosition, transform.rotation);
            Rigidbody rb = newObj.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = transform.up * 4f;
                rb.AddForce(transform.forward * throwBottleForce, ForceMode.Impulse);
            }

            Destroy(newObj, destroyBottleTime);
        }
    }
}
