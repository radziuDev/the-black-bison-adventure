using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnding : MonoBehaviour
{
    public TextAnimation textManager;
    public PlayerInteraction inventory;

    private void OnTriggerEnter(Collider collision) 
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && inventory.pengewenkiCount >= 10)
        {
            textManager.StartDialog(5);
        }
    }
}

