using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : MonoBehaviour
{
    public float damageAmount = 10f;
    private Statistics statistics;
    public Slider healthSlider;
    public float maxHP = 100;
    public float currentHP;

    public LayerMask playerLayer;
    public float pushForce = 10f;
    public float detectionRadius = 5f;
    private Transform player;
    public float maxVelocity = 2f;

    void Start()
    {
        statistics = GetComponent<Statistics>();
        currentHP = maxHP;
        healthSlider.maxValue = maxHP;
        healthSlider.minValue = 0;
        healthSlider.value = currentHP;

    }

    void Update()
    {
        healthSlider.value = currentHP;
        if (currentHP <= 0) {
            Destroy(gameObject);
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, detectionRadius, playerLayer);

        if (colliders.Length > 0)
        {
            player = colliders[0].transform;

            Vector3 direction = (player.position - transform.position).normalized;

            Rigidbody rb = GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddForce(direction * pushForce, ForceMode.Force);
            }

            if (rb.velocity.magnitude > maxVelocity)
            {
                rb.velocity = rb.velocity.normalized * maxVelocity;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("PlayerWeapon"))
        {
            TakeDmg(damageAmount);
        }
    }

    private void OnTriggerEnter(Collider collision) 
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("PlayerWeapon"))
        {
            TakeDmg(damageAmount);
        }
    }


    public void TakeDmg(float amount) {
        currentHP -= amount;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}