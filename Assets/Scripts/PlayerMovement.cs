using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public Transform feet;
    public float speed = 10f;
    private Vector2 movement;
    private Rigidbody rb;
    private bool isGrounded;
    public float jumpForce = 130f;
    public float rotateSpeed = 7f;
    public TextAnimation textManager;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (textManager.isDialog) {
            return;
        };
        movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        transform.forward = Vector3.Slerp(transform.forward, new Vector3 (movement.x, 0, movement.y), rotateSpeed * Time.deltaTime);
        isGrounded = Physics.Raycast(feet.position, Vector3.down, .1f);
    }

    private void FixedUpdate()
    {
        if (textManager.isDialog) {
            return;
        };

        MoveCharacter(movement);
            if (Input.GetKey(KeyCode.Space))
            {
                Jump();
            }
    }

    void MoveCharacter(Vector2 direction)
    {
        rb.MovePosition(transform.position + (new Vector3(direction.x, 0, direction.y) * speed * Time.deltaTime));
    }

    void Jump()
    {
        if (isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce);
        }
    }
}