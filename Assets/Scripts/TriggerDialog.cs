using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialog : MonoBehaviour
{
    public bool once = true;
    public bool onPress = true;
    public int dialogNumber = 0;

    private bool useed = false;
    public TextAnimation textManager;

    private void OnTriggerStay(Collider collision) 
    {
        if (useed) return;
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (onPress && !Input.GetKey(KeyCode.F)) return;
            useed = true;
            
            textManager.StartDialog(dialogNumber);
        }
    }

    private void OnTriggerExit(Collider collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (!once) {
                useed = false;
            }
        }
    }
}
