using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Statistics : MonoBehaviour
{
    public Slider healthSlider;
    public Slider staminaSlider;
    public Slider toxicitySlider;
    public TextAnimation textManager;

    public Image looseScreen;

    public TextMeshProUGUI bottlesUI;
    public TextMeshProUGUI emptyBottlesUI;
    
    public int maxHP = 100;
    public int currentHP;
    public int hpRegen = 1;
    
    public int maxToxicity = 50;
    public int currentToxicity;
    public int drainToxicityValue = 1;
    public float toxicityThresholdPercentage = 0.75f;

    public int maxStamina = 50;
    public int currentStamina;
    public int restoreStaminaValue = 3;

    public int bootles = 10;
    public int emptyBootles = 0;

    public bool gameOver = false;

    void Start()
    {
        currentHP = maxHP;
        currentToxicity = 0;
        currentStamina = maxStamina;

        healthSlider.maxValue = maxHP;
        staminaSlider.maxValue = maxStamina;
        toxicitySlider.maxValue = maxToxicity;

        healthSlider.minValue = 0;
        staminaSlider.minValue = 0;
        toxicitySlider.minValue = 0;

        healthSlider.value = currentHP;
        staminaSlider.value = currentStamina;
        toxicitySlider.value = currentToxicity;

        InvokeRepeating("RenewHp", 1f, 3f);
        InvokeRepeating("DrainToxicity", 1f, 2f);
        InvokeRepeating("RestoreStamina", 1f, 0.5f);
        InvokeRepeating("UpdateBar", 1f, 0.2f);
    }

    void Update()
    {
        if (currentHP <= 0 && !gameOver)
        {
            GameOver();
        }

        bottlesUI.text = bootles.ToString();
        emptyBottlesUI.text = emptyBootles.ToString();
    }

    void UpdateBar()
    {
        healthSlider.value = currentHP;
        staminaSlider.value = currentStamina;
        toxicitySlider.value = currentToxicity;
    }

    void RenewHp()
    {
        if (!gameOver)
        {
            currentHP += hpRegen;
            currentHP = Mathf.Clamp(currentHP, 0, maxHP);

            if ((float)currentToxicity / maxToxicity > toxicityThresholdPercentage)
            {
                ChangeHP(-10);
            }
        }
    }

    void DrainToxicity()
    {
        if (!gameOver)
        {
            currentToxicity -= drainToxicityValue;
            currentToxicity = Mathf.Clamp(currentToxicity, 0, maxToxicity);
        }
    }

    void RestoreStamina()
    {
        if (!gameOver)
        {
            currentStamina += restoreStaminaValue;
            currentStamina = Mathf.Clamp(currentStamina, 0, maxStamina);
        }
    }

    public void ChangeHP(int amount)
    {
        currentHP += amount;
        currentHP = Mathf.Clamp(currentHP, 0, maxHP);
    }

    public void ChangeToxicity(int amount)
    {
        currentToxicity += amount;
        currentToxicity = Mathf.Clamp(currentToxicity, 0, maxToxicity);
    }

    public void ChangeStamina(int amount)
    {
        currentStamina += amount;
        currentStamina = Mathf.Clamp(currentStamina, 0, maxStamina);
    }

    public bool CanIUseStamina(int amount)
    {
        return currentStamina > amount;
    }

    
    public bool CanIUseToxicity(int amount)
    {
        return currentToxicity > amount;
    }


    public void AddBootles(int amount)
    {
        bootles += amount;
    }
    
    public bool CanIDrink(int amount)
    {
        return bootles > 0;
    }

    public void Drink(int amount)
    {
        bootles -= amount;
        emptyBootles += amount;
    }

    public bool CanIThrow(int amount)
    {
        return emptyBootles > 0;
    }

    public void Throw(int amount)
    {
        emptyBootles -= amount;
    }

    public void GameOver()
    {
        if (!gameOver) {
            textManager.disabledModule = true;
            gameOver = true;
            textManager.isDialog = true;
            looseScreen.gameObject.SetActive(true);
            StartCoroutine(ExecuteAfterDelay(5f));
        }
    }

    IEnumerator ExecuteAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("MainMenu");        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            ChangeHP(-10);
        }
    }

}
